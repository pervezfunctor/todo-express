todo = require './todo'
actions = require './todo-routes'
todo_input_data = require './todo-data'

Q = require 'q'
_ = require 'underscore'
express_promise = require 'express-promise'
require 'express-resource'
express = require 'express'

app = express()

app.use express.static './public'

app.set "view engine", "jade"
app.set "views", "./views"

app.use express.json()
app.use express.urlencoded()
app.use express.methodOverride()

app.use express_promise()

app.use express.logger('dev')
app.resource 'todos', actions

#express_error = require 'express-error'
#app.use express_error.express3 contextLinesCount: 3, handleUncaughtException: true

#app.use (req, res) ->
#  res.status 401
#  res.send "cannot find resource"
#
#app.use (err, req, res, next) ->
#  res.status 501
#  res.send "Internal Server Error"

(Q.all _(todo_input_data).map (t) -> todo.insert t.title, t.completed)
.done -> app.listen 8080


