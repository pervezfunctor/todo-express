todo = require './todo'
q = require 'q'
_ = require 'underscore'

todos = [
  { title : 'Write F# code', completed : false }
  { title : 'Write ExpressJS code', completed : true }
  { title : 'Learn RxJS', completed : true }
  { title : 'Read SICP', completed : false }
]

(q.all _(todos).map (t) -> todo.insert t)
.then (all)->
    todos = all
    console.log "INSERTED ALL : "
    _(todos).each (t) -> console.log t.toString()

    todo.find todos[3].id
.then (t) ->
    console.log 'FOUND : ', t.toString()

    todo.update todos[0].id, true
.then (t) ->
    console.log 'UPDATED : ', t.toString()

    todo.remove todos[1].id
.then ->
    todo.all()
.then (todos) ->
    console.log 'INDEX : '
    _(todos).each (t) -> console.log t.toString()

.fail (err) ->
    console.error err
.done()



