todo = require './todo'

q = require 'q'
_ = require 'underscore'

express = require 'express'
app = express()
app.use express.static './public'
app.set "view engine", "jade"
app.set "views", "./views"

app.use express.json()
app.use express.urlencoded()
app.use express.methodOverride()


app.get '/todos/new', (req, res) ->
  res.render 'new'

app.post '/todos', (req, res) ->
  (todo.insert req.param('title'), req.param('completed') == 'yes')
  .done (t) ->
    res.redirect '/todos/' + t.id + '/show'

app.get '/todos', (req, res) ->
  (todo.all())
  .done (todos) ->
    res.render 'index', todos : todos


app.get '/todos/:id/show', (req, res) ->
  (todo.find req.param 'id')
  .done (todo) ->
    res.render 'show', todo

app.get '/todos/:id/edit', (req, res) ->
  (todo.find req.param 'id')
  .done (t) ->
    res.render 'edit', t

app.put '/todos/:id', (req, res) ->
  (todo.update req.param('id'), (req.param('completed') == 'yes'))
  .done (t) ->
    res.redirect '/todos/' + t.id + '/show'


app.del '/todos/:id', (req, res) ->
  (todo.remove req.param 'id')
  .done ->
    res.redirect '/todos'


(q.all _(require './todo-data').map (t) -> todo.insert t.title, t.completed)
.done ->
  app.listen 8080


