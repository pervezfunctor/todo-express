Q = require 'q'
_ = require 'underscore'

todos = []

insert_todo = (title, completed) ->
  t =
    id: 10001 + todos.length
    title : title
    completed : completed
    toString : -> '(' + @id + ', ' + @title + ', ' + @completed + ')'
  todos.push t
  t

find_todo = (id) ->
  todo = _(todos).find (todo) -> todo.id == +id
  throw new Error("cannot find todo with id : " + id) unless todo
  todo

set_completed = (id, completed) ->
  todo = find_todo id
  throw new Error("cannot update todo with id : " + id) unless todo
  todo.completed = completed
  todo

index_of = (id) ->
  for i in [0...todos.length]
    if todos[i].id == +id
      return i
  return -1

delete_todo = (id) ->
  index =  index_of id
  throw new Error("cannot delete todo with id : " + id) if index == -1
  todos.splice index, 1

all_todos = () -> todos

module.exports =
  insert : Q.fbind insert_todo
  find : Q.fbind find_todo
  remove : Q.fbind delete_todo
  all : Q.fbind all_todos
  update : Q.fbind set_completed
