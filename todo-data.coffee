module.exports = [
  { title : 'Write F# code', completed : false }
  { title : 'Write ExpressJS code', completed : true }
  { title : 'Learn RxJS', completed : true }
  { title : 'Read SICP', completed : false }
]
